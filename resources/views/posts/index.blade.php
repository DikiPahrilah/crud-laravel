@extends('layouts.app')

@section('title', 'Crud Data')

@section('content')

<a href="{{ route('posts.create') }}" class="btn btn-success btn-responsive" data-toggle="tooltip" data-placement="top" title="Tambah Data"><i class="fa fa-plus-circle"> Tambah Data</i></a>

@if(session()->get('success'))
    <div class="alert alert-success mt-3">
        {{ session()->get('success') }}
    </div>
@endif

<table id="table" class="table table-striped mt-3">
    <thead>
        <tr>
        <th scope="col">ID</th>
        <th scope="col">Product Name</th>
        <th scope="col">Product Description</th>
        <th scope="col">Product Price</th>
        <th scope="col">Product Qty</th>
        <th class="table-buttons">Action</th>
        </tr>
    </thead>
    <tbody>

   <?php $no = 0; ?>
    @foreach($posts as $post)

    <?php $no++ ;?>
        <tr>
        <th scope="row">{{ $no }}</th>
        <td>{{ $post->nama_product }}</td>
        <td>{{ $post->description }}</td>
        <td>Rp. {{ $post->price }}</td>
        <td>{{ $post->qty }}</td>

        <td class="table-buttons">
            <a href="{{ route('posts.edit', $post) }}" class="btn btn-primary btn-responsive" data-toggle="tooltip" data-placement="top" title="Edit">
                <i class="fa fa-edit"> Edit</i>
            </a>

            <a href="{{ route('posts.show', $post) }}" class="btn btn-success btn-responsive" data-toggle="tooltip" data-placement="top" title="Lihat">
                <i class="fa fa-eye"> Lihat</i>
            </a>

            <form action="{{ route('posts.destroy', $post) }}" method="POST">
            @csrf
            @method('DELETE')
                <button type="submit" class="btn btn-danger btn-responsive" onclick="return confirm('Hapus Data Ini ?')" data-toggle="tooltip" data-placement="top" title="Hapus">
                    <i class="fa fa-trash"> Delete</i>
                </button>
            </form>

            <!-- <button type="submit" class="btn btn-danger" onclick="deleteConfirmation({{$post->id}})" data-toggle="tooltip" data-placement="top" title="Hapus">
                <i class="fa fa-trash"> Delete</i>
            </button> -->

        </td>
        </tr>
    @endforeach
    </tbody>
 </table>

 {{ $posts->links() }}

 <style>
    @media (max-width: 768px) {
        .btn-responsive {
            padding:2px 4px;
            font-size:80%;
            line-height: 1;
            border-radius:3px;
        }
    }

    @media (min-width: 769px) and (max-width: 992px) {
        .btn-responsive {
            padding:4px 9px;
            font-size:90%;
            line-height: 1.2;
        }
    }
  </style> 

 <!-- <script type="text/javascript">
    $(document).ready( function () {
        $('#table').DataTable();
    } );

    function deleteConfirmation(id) {
        swal({
            title: "Delete?",
            text: "Please ensure and then confirm!",
            type: "warning",
            showCancelButton: !0,
            confirmButtontext: "Yes, delete it!",
            cancelButtonText: "No, cancel!",
            reverseButtons: !0
        }).then(function (e) {

            if (e.value === true) {
                var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');

                $.ajax({
                    type: 'POST',
                    url: "{{url('/delete')}}/" + id,
                    data: {_token: CSRF_TOKEN},
                    dataType: 'JSON',
                    success: function (results) {

                        if (results.success === true) {
                            swal("Done!", results.message, "success");
                        } else {
                            swal("Error", results.message, "error");
                        }
                    }
                });
            } else {
                e.dismiss;
            }
        }, function (dismiss) {
            return false;
        })
    }

</script> -->

@endsection

