@extends('layouts.app')

@section('title', 'Tambah Data')

@section('content')
<div class="row">
<div class="col-lg-6 mx-auto">
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form method="POST" action="{{ route('posts.store') }}">
    @csrf
        <div class="form-group">
            <label for="post-name">Nama Produk</label>
            <input type="text" name="nama_product" value="{{ old('nama_product') }}" class="form-control" id="nama_product" placeholder="Nama Produk">
        </div>
        <div class="form-group">
            <label for="post-description">Description</label>
            <textarea name="description" class="form-control" id="description" placeholder="Masukkan Deskripsi">{{ old('description') }}</textarea>
        </div>
        <div class="form-group">
            <label for="post-price">Price</label>
            <input type="text" name="price" value="{{ old('price') }}"class="form-control" id="price" placeholder="Price">
        </div>
        <div class="form-group">
            <label for="post-qty">Qty</label>
            <input type="text" name="qty" value="{{ old('qty') }}" class="form-control" id="qty" placeholder="Qty">
        </div>
        <button type="submit" class="btn btn-success btn-responsive">Simpan Data</button>
        <a class="btn btn-primary btn-responsive" href="{{ route('posts.index') }}"> Cancel</a>
    </form>

    <style>
        @media (max-width: 768px) {
            .btn-responsive {
                padding:2px 4px;
                font-size:80%;
                line-height: 1;
                border-radius:3px;
            }
        }

        @media (min-width: 769px) and (max-width: 992px) {
            .btn-responsive {
                padding:4px 9px;
                font-size:90%;
                line-height: 1.2;
            }
        }
  </style>
</div>
</div>
@endsection