@extends('layouts.app')

@section('title', $post->nama_product)

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Data</div>

                <div class="card-body">
                    <h3>{{ $post->nama_product }}</h3>
                    <p>{{ $post->description }}</p>
                    <p><b>Rp. {{ $post->price }}</b></p>
                    <p>{{ $post->qty }}</p>

                    <a class="btn btn-primary btn-responsive" href="{{ route('posts.index') }}"> Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    @media (max-width: 768px) {
        .btn-responsive {
            padding:2px 4px;
            font-size:80%;
            line-height: 1;
            border-radius:3px;
        }
    }

    @media (min-width: 769px) and (max-width: 992px) {
        .btn-responsive {
            padding:4px 9px;
            font-size:90%;
            line-height: 1.2;
        }
    }
</style>
@endsection
