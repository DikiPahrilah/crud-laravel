@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <h4>Selamat datang, <b>{{ Auth::user()->email }}</b> </h4>
                    <br/>
                    <a class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="Tambah Data" href="{{ route('posts.index') }}"><i class="fa fa-plus-circle"> Add A More Products</i></a>

                    <a class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Logout" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        <i class="fa fa-sign-out">{{ __('Logout') }}</i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
