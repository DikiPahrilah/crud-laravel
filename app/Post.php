<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'nama_product', 'description', 'price', 'qty',
    ];
}
