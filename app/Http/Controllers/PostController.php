<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $posts = Post::all();
        // dd($posts);

        // $posts = Post::orderBy('created_at', 'desc')->paginate(5);

        $posts = Post::paginate(5);

        return view('posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_product'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required|max:255',
            'qty'=> 'required|max:255'
        ]);

        $post = new Post([
            'nama_product' => $request->get('nama_product'),
            'description' => $request->get('description'),
            'price' => $request->get('price'),
            'qty' => $request->get('qty')
        ]);

        $post->save();

        return redirect('/posts')->with('success', 'Data Berhasil Disimpan!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::find($id);

        return view('posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama_product'=> 'required|max:255',
            'description'=> 'required|max:255',
            'price'=> 'required|max:255',
            'qty'=> 'required|max:255'
        ]);

        $post = Post::find($id);
        $post->nama_product = $request->get('nama_product');
        $post->description = $request->get('description');
        $post->price = $request->get('price');
        $post->qty = $request->get('qty');
        $post->save();

        return redirect('/posts')->with('success', 'Data Berhasil Diedit!.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::find($id);
        $post->delete();

        return redirect('/posts')->with('success', 'Data Berhasil Dihapus!.');
    }


    // public function delete($id)
    // {
    //     $delete = Post::where('id', $id)->delete();

    //     // check data deleted or not
    //     if ($delete == 1){
    //         $success = true;
    //         $message = "Data Berhasil Dihapus !.";
    //     } else {
    //         $success = true;
    //         $message = "Data not found";
    //     }

    //     // Return response
    //     return response()->json([
    //         'success' => $success,
    //         'message' => $message,
    //     ]);

    //     redirect('/posts')->with('success', 'Data Berhasil Dihapus!.');

    // }
}
